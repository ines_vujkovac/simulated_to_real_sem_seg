import os
from PIL import Image
from skimage.io import imshow, imread
import numpy as np

# current color composition:
'''
RGB
blue - (6, 71, 207) - environment
gray - (38, 38, 34) - end-effector
green - (0, 96, 0) - robot
light blue - (0, 138, 207) - conveyor
orange - (204, 141, 14) - tray
red - (206, 0, 11) - cell (fence and things around robot)
yellow - (208, 207, 63) - default (unlabeled)
purple - (219, 0, 174) - camera
'''

# current indexes
'''
# add indexes
robot_ind = 1
person_ind = 2
fence_ind = 3
tray_empty_ind = 4
tray_ind = 5
part_ind = 6
rails_ind = 7
conveyor_id = 8
empty_ind = 0
'''

image_files = sorted(os.listdir('segment_png_pairs/'))

for image_file in image_files:
    seg = imread('segment_png_pairs/%s' % image_file)
    mask = seg

    # isolate green - robot
    filtered_ind = np.where((mask[:,:,0]==0) & (mask[:,:,1]==96) & (mask[:,:,2]==0))
    seg[filtered_ind] = 25 #1

    # isolate red - fence
    filtered_ind = np.where((mask[:,:,0]==206) & (mask[:,:,1]==0) & (mask[:,:,2]==11))
    seg[filtered_ind] = 13 #3

    # isolate orange - tray
    filtered_ind = np.where((mask[:,:,0]==204) & (mask[:,:,1]==141) & (mask[:,:,2]==14))
    seg[filtered_ind] = 8 #4

    # isolate light blue - conveyor
    filtered_ind = np.where((mask[:,:,0]==0) & (mask[:,:,1]==138) & (mask[:,:,2]==207))
    seg[filtered_ind] = 26 #8

    # isolate unlabeled - so far
    m = (seg != 25) & (seg != 13) & (seg != 8) & (seg != 16)
    seg[m] = 0

    image = Image.fromarray(seg[:,:,1])
    image.save('mask_png/%s' % image_file)