"""
clean files, to keep only mathcing filenames -> 
real to segmented pairs
"""

import os
import shutil

real = os.listdir('real_png')
segment = os.listdir('segment_png')

print('real: {}'.format(len(real)))
print('segment: {}'.format(len(segment)))

pairs_real = []
pairs_seg = []

for seg in segment:
    tmp = seg.split('_')
    tmp[0] = 'real'
    tmp = ('_').join(tmp)
    if tmp in real:
        pairs_real.append(tmp)
        pairs_seg.append(seg)


for pair_real, pair_seg in zip(pairs_real, pairs_seg):
    file_to_copy_real = 'real_png/' + pair_real
    file_to_copy_seg = 'segment_png/' + pair_seg
    shutil.copy(file_to_copy_real, 'real_png_pairs/') 
    shutil.copy(file_to_copy_seg, 'segment_png_pairs/') 


# check lengths
real = os.listdir('real_png_pairs')
segment = os.listdir('segment_png_pairs')

print('real pairs: {}'.format(len(real)))
print('segment: {}'.format(len(segment)))